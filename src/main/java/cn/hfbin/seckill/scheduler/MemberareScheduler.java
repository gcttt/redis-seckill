package cn.hfbin.seckill.scheduler;


import cn.hfbin.seckill.dao.PhoneRankMapper;
import cn.hfbin.seckill.dto.RankDto;
import cn.hfbin.seckill.entity.PhoneRank;
import cn.hfbin.seckill.enums.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**定时任务：定时自动刷新redis中的数据，保证数据一致性
 **/
@Component
public class MemberareScheduler {

    private static final Logger log= LoggerFactory.getLogger(MemberareScheduler.class);
    @Resource
    private PhoneRankMapper phoneRankMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    //此处表示，每 15秒执行一次
    @Scheduled(cron = "0/15 * * * * ?")
    public void sortFareScheduler(){
        //定时执行以下命令
        this.cacheSortResult();
        System.out.println("定时任务--刷新redis");
    }

    //异步方法：@Async是Spring的注解，可以加在类或方法上。
    // 如果加上了这个注解，那么该类或者该方法在使用时将会进行异步处理，
    // 也就是创建一个线程来实现这个类或者方法，实现多线程。
//    @Async("threadPoolTaskExecutor")
    void cacheSortResult(){
        try {
            ZSetOperations<String, RankDto> zSetOperations=redisTemplate.opsForZSet();

            List<PhoneRank> list= phoneRankMapper.getAllSortRank();
            if (list!=null && !list.isEmpty()){
                redisTemplate.delete(Constant.RedisSortedSetKey);

                list.forEach(rank -> {
                    RankDto dto=new RankDto(rank.getGoodsName());
                    zSetOperations.add(Constant.RedisSortedSetKey,dto, rank.getSellCount().doubleValue());
                });
            }
        }catch (Exception e){
            log.error("发生异常：",e.fillInStackTrace());
        }
    }
}

































