package cn.hfbin.seckill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * dto实体类：用于业务类
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RankDto implements Serializable {

    private String goodsName;
}
