package cn.hfbin.seckill.service.ipml;

import cn.hfbin.seckill.entity.User;
import cn.hfbin.seckill.dao.UserMapper;
import cn.hfbin.seckill.param.LoginParam;
import cn.hfbin.seckill.result.CodeMsg;
import cn.hfbin.seckill.result.Result;
import cn.hfbin.seckill.service.UserService;
import cn.hfbin.seckill.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapper userMapper;

//    @Resource
//    private PasswordEncoder passwordEncoder;

    @Override
    public Result<User> login(LoginParam loginParam) {

        User user = userMapper.checkPhone(loginParam.getMobile());
        if(user == null){
            return Result.error(CodeMsg.MOBILE_NOT_EXIST);
        }
        String dbPwd= user.getPassword();
        System.out.println("正确密码dbpwd: "+dbPwd);
        String saltDB = user.getSalt();
        System.out.println("saltdb: "+saltDB);
        String calcPass = MD5Util.formPassToDBPass(loginParam.getPassword(), saltDB);
        System.out.println("输入密码加密一次: "+calcPass);

        String calcPass2 = MD5Util.formPassToDBPass(calcPass, saltDB);
        System.out.println("输入密码加密二次: "+calcPass2);
        System.out.println("判断输入密码与正确密码是否一致:");
        if(!StringUtils.equals(dbPwd , calcPass2)){
            return Result.error(CodeMsg.PASSWORD_ERROR);
        }
        user.setPassword(StringUtils.EMPTY);
        System.out.println("登录成功");
        return Result.success(user);

//        User user = userMapper.checkPhone(loginParam.getMobile());
//        if(user == null){
//            return Result.error(CodeMsg.MOBILE_NOT_EXIST);
//        }
//        String dbPwd= user.getPassword();
//        System.out.println("pwd: "+dbPwd);
//        String saltDB = user.getSalt();
//        System.out.println("saltdb: "+saltDB);
//        if(!passwordEncoder.matches(dbPwd,loginParam.getPassword())){
//            return Result.error(CodeMsg.PASSWORD_ERROR);
//        }
//        user.setPassword(StringUtils.EMPTY);
//        System.out.println("登录成功");
//        return Result.success(user);
    }

//    public Result<User> logout(LogoutParam logoutParam) {
//    }

}
