package cn.hfbin.seckill.service.ipml;

import cn.hfbin.seckill.dao.PhoneRankMapper;
import cn.hfbin.seckill.dto.RankDto;
import cn.hfbin.seckill.entity.PhoneRank;
import cn.hfbin.seckill.enums.Constant;
import cn.hfbin.seckill.service.PhoneRankService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Service
public class PhoneRankServiceImpl implements PhoneRankService {

    @Resource
    private PhoneRankMapper phoneRankMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    //1: 改进：新增销售时，要考虑用户是否已经被销售了，如果是就要加总计算
    //RankDto实体类，单独存储商品手机名，根据商品手机名字判断是否已销售过
    //开启事务
    @Transactional(rollbackFor = Exception.class)
    public Long addRank(PhoneRank rank) throws Exception {
        //先判断数据库有没有数据，有销售记录的话，数据库就update
        PhoneRank tmp= phoneRankMapper.selectByGoodsName(rank.getGoodsName());
        //如果没有相应记录，就在数据库中新增条目
        if(tmp == null){
            //1.写入数据库
            int res = phoneRankMapper.insertSelective(rank);
            //        int res=phoneRankMapper.insert(rank);
            if (res>0){
                //2.用RankDto存储销售过的手机
                RankDto dto=new RankDto();
                dto.setGoodsName(rank.getGoodsName());
                ZSetOperations<String,RankDto> zSetOperations=redisTemplate.opsForZSet();
                //3.查询该手机对应的分数
//                Double oldRank = zSetOperations.score(Constant.RedisSortedSetKey,dto);
//                if (oldRank == null){
                    //4.表示之前该手机被销售过了，需要进行叠加,incrementScore方法就是增加分数的
                    // zSetOperations.incrementScore(
                    // Constant.RedisSortedSetKey,dto,rank.getSellCount().doubleValue());
                    // }else{
                    //5: 表示被销售一次，就写入redis
//                    zSetOperations.add(Constant.RedisSortedSetKey,dto,rank.getSellCount().doubleValue());
//                }
            }
        }
        else{
            //数据库中有相应的条目，就进行更新数据库中销量，不插入数据库
            BigDecimal olddata =tmp.getSellCount();                    //数据库中的销量
            BigDecimal mid = rank.getSellCount();            //要加的数量
            BigDecimal newdata = olddata.add(mid);     //更新后的销量
            rank.setSellCount(newdata);
            int res2 = phoneRankMapper.updateByGoodsName(rank);

            //更新redis中的数据
            if (res2>0){
                //2.用RankDto存储销售过的手机
                RankDto dto=new RankDto(rank.getGoodsName());
                ZSetOperations<String,RankDto> zSetOperations=redisTemplate.opsForZSet();
                //3.查询该手机对应的分数
                Double oldRank = zSetOperations.score(Constant.RedisSortedSetKey,dto);
                if (oldRank != null){
                    //4.表示之前该手机被销售过了，需要进行叠加,incrementScore方法就是增加分数的
                    zSetOperations.incrementScore(
                            Constant.RedisSortedSetKey,dto,mid.doubleValue());
                }
//                else{
//                    //5: 表示被销售一次，就写入redis
//                    zSetOperations.add(Constant.RedisSortedSetKey,dto,mid.doubleValue());
//                }
            }

        }
        return rank.getId();
    }



    public List<PhoneRank> getAllRank() {
        List<PhoneRank> list= Lists.newLinkedList();
        final String key= Constant.RedisSortedSetKey;
        ZSetOperations<String, RankDto> zSetOperations=redisTemplate.opsForZSet();
        final Long size=zSetOperations.size(key);
        //反向取出所有数据，包括分数
        Set<ZSetOperations.TypedTuple<RankDto>> set=
                zSetOperations.reverseRangeWithScores(key,0L,size);
        if (set!=null && !set.isEmpty()){
            set.forEach(tuple -> {
                PhoneRank rank=new PhoneRank();
                rank.setSellCount(BigDecimal.valueOf(tuple.getScore()));
                rank.setGoodsName(tuple.getValue().getGoodsName());
                list.add(rank);
            });
        }
        return list;
    }


    public List<PhoneRank> getRangeRank(double minRange, double maxRange) {
        List<PhoneRank> list= Lists.newLinkedList();
        //设置key
        final String key=Constant.RedisSortedSetKey;
        ZSetOperations<String, RankDto> zSetOperations=redisTemplate.opsForZSet();

        Set<ZSetOperations.TypedTuple<RankDto>> set=
                zSetOperations.rangeByScoreWithScores(key,minRange,maxRange);
        if (set!=null && !set.isEmpty()){
            set.forEach(tuple -> {
                PhoneRank rank=new PhoneRank();
                rank.setSellCount(BigDecimal.valueOf(tuple.getScore()));
                rank.setGoodsName(tuple.getValue().getGoodsName());
                list.add(rank);
            });
        }
        return list;
    }


}
