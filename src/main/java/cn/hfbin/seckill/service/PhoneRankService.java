package cn.hfbin.seckill.service;


import cn.hfbin.seckill.entity.PhoneRank;

import java.util.List;

public interface PhoneRankService {

    public Long addRank(PhoneRank phoneRank) throws Exception;
    public List<PhoneRank> getAllRank();
    public List<PhoneRank> getRangeRank(double minRange, double maxRange);

}
