package cn.hfbin.seckill.dao;


import cn.hfbin.seckill.entity.PhoneRank;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface PhoneRankMapper {

    //----mybatis
    int deleteByPrimaryKey(Long id);
    int insert(PhoneRank record);
    int insertSelective(PhoneRank record);
    PhoneRank selectByPrimaryKey(Long id);
    PhoneRank selectByGoodsName(String goodsname);
    int updateByPrimaryKeySelective(PhoneRank record);
    int updateByPrimaryKey(PhoneRank record);
    int updateByGoodsName(PhoneRank record);

    Set<PhoneRank> sortAllRank(Integer isAsc);
//    3.优化：基于数据库记录的补偿排名机制
    List<PhoneRank> getAllSortRank();



}
