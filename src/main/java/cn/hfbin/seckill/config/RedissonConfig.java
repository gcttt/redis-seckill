package cn.hfbin.seckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {
    @Bean
    public RedissonClient redissonClient(){
        //配置
        Config config = new Config();
//        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
//        集群配置
        config.useClusterServers().addNodeAddress("redis://120.25.223.26:6380",
                "redis://120.25.223.26:6381",
                "redis://120.25.223.26:6382",
                "redis://120.25.223.26:6383",
                "redis://120.25.223.26:6384",
                "redis://120.25.223.26:6385");
        //创建 RedissonClient 对象
        return Redisson.create(config);
    }
}