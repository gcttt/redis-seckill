package cn.hfbin.seckill.mq;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MQConfig {
	
	public static final String MIAOSHA_QUEUE = "seckill.queue";
	public static final String QUEUE = "queue";

	///////
	@Bean
	public Queue miaoShaQueue(){
		return new Queue(MIAOSHA_QUEUE,true);
	}
	//////
	@Bean
	public MessageConverter getMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
	@Bean
	public Queue queue() {
		return new Queue(QUEUE, true);
	}
	
}
