package cn.hfbin.seckill.mqRedis;

import cn.hfbin.seckill.mq.MQSender;
import cn.hfbin.seckill.mq.SeckillMessage;
import cn.hfbin.seckill.redis.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @projectName: Seckill
 * @package: cn.hfbin.seckill.mqRedis
 * @className: Sender
 * @author: GCT
 * @description: TODO
 * @date: 2022/12/28 12:25
 * @version: 1.0
 */
@Service
public class Sender {

    private static Logger log = LoggerFactory.getLogger(Sender.class);


    @Autowired
    private RedisTemplate redisTemplate;

    //创建通告
    @Transactional(rollbackFor = Exception.class)
    public void sendToSeckillMessageQueue(SeckillMessage mm){

        String msg = RedisService.beanToString(mm);
        log.info("send message:" + msg);
        ListOperations<String,String> listOperations=redisTemplate.opsForList();
        listOperations.leftPush(Constant.RedisListNoticeKey,msg);
        System.out.println("写入redis成功");

    }
}
