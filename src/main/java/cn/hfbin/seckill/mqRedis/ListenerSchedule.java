package cn.hfbin.seckill.mqRedis;

import cn.hfbin.seckill.bo.GoodsBo;
import cn.hfbin.seckill.entity.SeckillOrder;
import cn.hfbin.seckill.entity.User;
import cn.hfbin.seckill.mq.SeckillMessage;
import cn.hfbin.seckill.redis.RedisService;
import cn.hfbin.seckill.service.OrderService;
import cn.hfbin.seckill.service.SeckillGoodsService;
import cn.hfbin.seckill.service.SeckillOrderService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @projectName: Seckill
 * @package: cn.hfbin.seckill.mqRedis
 * @className: ListenerSchedule
 * @author: GCT
 * @description: TODO
 * Redis列表-队列的消费者监听器
 * @date: 2022/12/28 12:06
 * @version: 1.0
 */
@Component
@EnableScheduling
public class ListenerSchedule {
    private static final Logger log = LoggerFactory.getLogger(ListenerSchedule.class);
    private static final String listenKey = Constant.RedisListNoticeKey;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    RedisService redisService;

    @Autowired
    SeckillGoodsService goodsService;

    @Autowired
    OrderService orderService;

    @Autowired
    SeckillOrderService seckillOrderService;

    private static final ExecutorService executorService = Executors.newFixedThreadPool(4);

    //1.定时任务检测
    @Scheduled(cron = "0/2 * * * * ?")
    public void schedulerListenMessage() {
        log.info("----定时任务调度队列监听、检测通告消息，监听list中的数据");

        ListOperations<String, String> listOperations = redisTemplate.opsForList();

        //1.1：从右侧弹出一条通告,弹出后在list中就删除了该条数据
        String msg = listOperations.rightPop(listenKey);

        log.info("receive message(msg):" + msg);

        while (msg != null) {
            SeckillMessage mm = RedisService.stringToBean(msg, SeckillMessage.class);

            User user = mm.getUser();
            long goodsId = mm.getGoodsId();

            GoodsBo goods = goodsService.getseckillGoodsBoByGoodsId(goodsId);
            int stock = goods.getStockCount();
            if (stock <= 0) {
                return;
            }
            //判断是否已经秒杀到了
            SeckillOrder order = seckillOrderService.getSeckillOrderByUserIdGoodsId(user.getId(), goodsId);
            if (order != null) {
                return;
            }
            //减库存 下订单 写入秒杀订单
            seckillOrderService.insert(user, goods);

//            //1.3.继续判断list中是否有通告
            msg = listOperations.rightPop(listenKey);
            log.info("receive message(msg in while):" + msg);
        }

//    //2:给不同的商户发送通知
//    //从之前创建的线程池中取线程
//    @Async("threadPoolTaskExecutor")
//    void noticeUser(Notice notice) {
//        if (notice != null) {
//            //2.1：查询出所有用户
//            List<User> list = userMapper.selectList();
//
//            //2.2 方法1:串行发送邮件
////           if (list!=null && !list.isEmpty()){
////                list.forEach(user -> emailService.emailUserNotice(notice,user));
////            }
//
//            //2.2 方法2：线程池/多线程触发 发送邮件
//            try {
//                if (list != null && !list.isEmpty()) {
//                    //线程池里固定4个线程
//                    //   private static final  ExecutorService executorService = Executors.newFixedThreadPool(4);
//                    List<NoticeThread> threads = Lists.newLinkedList();  //任务集合
//                    list.forEach(user -> {
//                        threads.add(new NoticeThread(user, notice, emailService));
//                    });
//                    //线程池批量执行任务
//                    executorService.invokeAll(threads);
//                }
//            } catch (Exception e) {
//                log.error("近实时的定时任务检测-发送通知给到不同的商户-法二-线程池/多线程触发-发生异常：", e.fillInStackTrace());
//            }
//
//        }
//
//    }

    }
}
