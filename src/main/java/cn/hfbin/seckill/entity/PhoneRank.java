package cn.hfbin.seckill.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneRank implements Serializable {

    private Long id;

    private Long goodsId;           //商品id

    private String goodsName;       //商品名      //使用前提，在yml文件中开启驼峰命名映射,mybatis-plus会进行相应的处理

    private BigDecimal sellCount;      //商品销量统计
}