package cn.hfbin.seckill.controller;

import cn.hfbin.seckill.entity.PhoneRank;
import cn.hfbin.seckill.result.CodeMsg;
import cn.hfbin.seckill.result.Result;
import cn.hfbin.seckill.service.PhoneRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

//改进版

@Controller
@RequestMapping("/rank")
public class PhoneRankController {

    @Autowired
    private PhoneRankService phoneRankService;

    //1.新增销售记录 url: http://localhost:8080/addRank
    @PostMapping("/addRank")
    public Result addGoodsRank(@RequestBody @Validated PhoneRank phoneRank, BindingResult result){

        Result response = new Result(CodeMsg.SUCCESS);
        try {
            response.setData(phoneRankService.addRank(phoneRank));
        }catch (Exception e){
            response=new Result(CodeMsg.SERVER_ERROR.getCode(),e.getMessage());
        }
        return response;
    }


    //2.获取完整的销售排行榜
    @GetMapping ( "/allRank")
    public Result getAllRank(){

        Result response = new Result(CodeMsg.SUCCESS);
        try {
            response.setData(phoneRankService.getAllRank());
        }catch (Exception e) {
            response=new Result(CodeMsg.SERVER_ERROR.getCode(),e.getMessage());
        }
        return response;
    }

    //3.获取区间销售排行榜
    @PostMapping ( "/rangeRank")
    public Result getRange(Double minRange,Double maxRange){
        Result response = new Result(CodeMsg.SUCCESS);
        Map<String,Object> resMap=new HashMap();
        try {
            resMap.put("SortRankRange",phoneRankService.getRangeRank(minRange,maxRange));
        }catch (Exception e) {
            response=new Result(CodeMsg.SERVER_ERROR.getCode(),e.getMessage());
        }
        response.setData(resMap);
        return response;
    }


}
